# Overview
The “Auto Combine” button is an unofficial addon for Terra Dotta that enables a user to quickly use the Combine Queries and Reports functionality for multiple queries/reports.

This is accomplished by using a Tampermonkey userscript to add an "Auto Combine" button to the Admin Home page that will perform the "combine queries and reports" function for all checked queries and reports. You can check multiple queries and multiple reports. For example, say you have 3 queries for different programs that you need to combine into a STEP report and an insurance report. Simply check the boxes next to each query and report and the script will combine them all for you.

# Requirements
You must have the [Tampermonkey extension](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en) installed in the Google Chrome web browser. Scripts may work in Greasemonkey on Firefox with little tweaking, but there are no guarantees.

# Installation

1. Ensure you have the [Tampermonkey extension](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en) installed in Google Chrome.
2. Click on this [installation link](https://bitbucket.org/gwoip/tampermonkey-scripts/raw/8ec65644334393725fbaf71e5beaebb755104169/MassCombineQueriesAndReports.user.js).
3. Click the Install button.